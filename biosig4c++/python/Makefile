####### Makefile for "BioSig for C/C++" #####################
###
###  Copyright (C) 2006-2020 Alois Schloegl <alois.schloegl@ist.ac.at>
###  This file is part of the "BioSig for C/C++" repository 
###  (biosig4c++) at http://biosig.sf.net/ 
###
##############################################################

PYTHON ?= python3
PYVER  := $(shell $(PYTHON) -c "import sys; print(sys.version[:3])")


release build target: dist/Biosig-2.1.0.tar.gz

# https://packaging.python.org/tutorials/packaging-projects/
dist/Biosig-2.1.0.tar.gz: setup.py biosigmodule.c
	$(PYTHON) setup.py sdist
	-python2 setup.py bdist_egg
	-python3 setup.py bdist_egg

install:
	$(PYTHON) setup.py install

test:
	## get ../data/Newtest17-256.bdf
	make -C .. fetchdata
	## need to run from different directory, because
	(cd .. && PYTHONPATH=/usr/local/lib/python$(PYVER)/dist-packages/ $(PYTHON) -c 'import biosig; print(biosig.header("data/Newtest17-256.bdf"))')
	(cd .. && PYTHONPATH=/usr/local/lib/python$(PYVER)/dist-packages/ $(PYTHON) -c 'import biosig; print(biosig.data("data/Newtest17-256.bdf"))')
	(cd .. && PYTHONPATH=/usr/local/lib/python$(PYVER)/dist-packages/ python$(PYVER) < python/demo2.py)

clean:
	-rm -rf build/*
	-rm -rf dist/*
	-rm *.so

check: release
	twine check dist/*

