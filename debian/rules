#!/usr/bin/make -f
# -*- makefile -*-

MAKEOPTS=LIBEXT=so

export PYBUILD_NAME=biosig
export PYBUILD_SYSTEM=pybuild

export DEB_CFLAGS_MAINT_APPEND  = -I$(CURDIR)

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

%:
	dh  $@ --with python3

override_dh_auto_configure:
	dh_auto_configure
	# : # Use our copies
	# ln -s debian/upstream-extern extern

override_dh_auto_build:
	: # Build everything upstream builds by default
	[ ! -f Makefile ] || $(MAKE) distclean
	./configure --prefix=/usr --libdir=/usr/lib/$(DEB_HOST_MULTIARCH)
	make $(MAKEOPTS) lib tools octave
	: # Assure additional interesting to us pieces are built : biosig for python
	cd biosig4c++/python && $(shell py3versions -d) setup.py sdist
	#make libbiosig.a save2gdf save2aecg save2scp pdp2gdf \
	#	 biosig_client biosig_server

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	echo "I: skip testing for now -- relies on download of test data"
endif

# Manual crafting of installing Octave and Python bindings
OCTDIR=$(shell octave-config -p LOCALOCTFILEDIR)/biosig
B4MDIR=$(shell octave-config -p LOCALFCNFILEDIR)

override_dh_auto_install:
	dh_auto_install

	: I: install Python binding for current Python version
	# make $(MAKEOPTS) -C biosig4c++/python install
	$(cd biosig4c++/python && foreach py,$(shell py3versions -d), (${py} setup.py install --prefix=debian/${py}-biosig) )
	find debian -name "biosig*.so"

	: I: install Octave bindings
	mkdir -p debian/octave-biosig$(OCTDIR)
	mkdir -p debian/octave-biosig$(B4MDIR)
	: # install with executable flag so dh_{strip,...} tools could catch it to operate
	install biosig4c++/mex/*.mex debian/octave-biosig$(OCTDIR)
	-rm biosig4matlab/t200_FileAccess/str2double.m
	cp -r biosig4matlab debian/octave-biosig$(B4MDIR)/biosig

override_dh_install:
	dh_install
	d-shlibmove --commit \
		    --multiarch \
		    --devunversioned \
		    --exclude-la \
		    --override s/libcholmod3-dev/libsuitesparse-dev/ \
		    --override s/libtinyxml2.6.2-dev/libtinyxml-dev/ \
		    --movedev debian/tmp/usr/include usr \
		    --movedev "debian/tmp/usr/lib/*/pkgconfig/*.pc" usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig \
		    debian/tmp/usr/lib/*/*.so

# No idea whether octave needs the extra LICENSE file - if so it can be deleted here
#override_dh_installdocs:
#	dh_installdocs
#	find debian -name LICENSE -delete

override_dh_missing:
	dh_missing --list-missing

override_dh_makeshlibs:
	dh_makeshlibs
	: # Provide octave:Depends
	dh_octave_substvar
	dh_numpy3

override_dh_fixperms:
	dh_fixperms
	chmod a-x debian/octave-biosig$(OCTDIR)/*.mex
	find debian -name "*.pc" -exec chmod -x \{\} \;

override_dh_auto_clean:
	dh_auto_clean
	if [ -f Makefile ] ; then make $(MAKEOPTS) clean ; fi 	# distclean invoked by auto isn't enough
	-rm extern
