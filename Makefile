####### Main Makefile for building the various biosig tools ####################
###
###  Copyright (C) 2010-2020 Alois Schloegl <alois.schloegl@gmail.com>
###  This file is part of the "BioSig for C/C++" repository
###  (biosig4c++) at http://biosig.sf.net/
###
################################################################################

exec_prefix ?= $(prefix)

first :: lib tools

java: lib
	make -C biosig4c++/java

python: lib
	make -C biosig4c++/python

lib:
	make -C biosig4c++ lib

tools: lib
	make -C biosig4c++ tools

mma mathematica: lib
	make -C biosig4c++/mma

mex4m matlab: lib
	make -C biosig4c++/mex mex4m

mex4o octave: lib
	make -C biosig4c++/mex mex4o

R: lib
	make -C biosig4c++/R

install ::
	make -C biosig4c++ install
	install -d $(DESTDIR)/usr/local/share/biosig/matlab
	cp -r biosig4matlab/* $(DESTDIR)/usr/local/share/biosig/matlab/
	-rm -rf $(DESTDIR)/usr/local/share/biosig/matlab/maybe-missing

uninstall ::
	make -C biosig4c++ uninstall
	-rm -rf $(DESTDIR)/usr/local/share/biosig

clean ::
	make -C biosig4c++/mma clean
	make -C biosig4c++ clean

distclean : clean
        # also configure.ac for list of files
	rm Makefile biosig4c++/Makefile biosig4c++/*/Makefile \
		biosig4c++/python/setup.py \
		biosig4c++/R/DESCRIPTION

ifneq (:,/usr/bin/java)
ifneq (:,/usr/bin/javac)
first :: lib
	-make -C biosig4c++/java
endif
clean ::
	-make -C biosig4c++/java clean
test ::
	-make -C biosig4c++/java test
endif

ifneq (:,/usr/local/bin/mathematica)
first :: lib
	make -C biosig4c++/mma
install ::
	-make -C biosig4c++/mma install
uninstall ::
	-make -C biosig4c++/mma uninstall
endif

ifneq (:,:)
MATLAB_MEX := $(shell dirname :)/mex
first ::
	make -C biosig4c++/mex mex4m
install ::
	make -C biosig4c++/mex install
uninstall ::
	make -C biosig4c++/mex uninstall
endif

ifneq (:,/usr/bin/octave)
ifneq (:,/usr/bin/mkoctfile)
BIOSIG_MEX_DIR = $(DESTDIR)$(shell octave-config -p LOCALOCTFILEDIR)/biosig
BIOSIG_DIR     = $(DESTDIR)$(shell octave-config -p LOCALFCNFILEDIR)/biosig
first ::
	make octave
	make -C biosig4c++ mexbiosig
install ::
	# mexbiosig
	#-/usr/bin/octave --no-gui --eval "pkg install -global biosig4c++/mex/mexbiosig-2.1.0.src.tar.gz"
	# *.mex
	install -d $(BIOSIG_MEX_DIR)
	install biosig4c++/mex/*.mex $(BIOSIG_MEX_DIR)
	# biosig for octave and matlab
	install -d $(BIOSIG_DIR)
	cp -r biosig4matlab/*  $(BIOSIG_DIR)
	-rm -rf $(BIOSIG_DIR)/maybe-missing

uninstall ::
	# mexbiosig
	#-/usr/bin/octave --no-gui --eval "pkg uninstall -global mexbiosig"
	# *.mex
	-rm -rf $(BIOSIG_MEX_DIR)
	# biosig for octave and matlab
	-rm -rf $(BIOSIG_DIR)
endif
endif

ifneq (:,/usr/bin/python)
first biosig4c++/python/dist/Biosig-2.1.0.tar.gz ::
	-PYTHON=/usr/bin/python make -C biosig4c++/python dist/Biosig-2.1.0.tar.gz
install :: biosig4c++/python/dist/Biosig-2.1.0.tar.gz
	-/usr/bin/python -m pip install $<
uninstall ::
	-/usr/bin/python -m pip uninstall -y Biosig
clean ::
	make -C biosig4c++/python clean
endif

ifneq (:,/usr/bin/python2)
install :: biosig4c++/python/dist/Biosig-2.1.0.tar.gz
	-/usr/bin/python2 -m pip install $<
uninstall ::
	-/usr/bin/python2 -m pip uninstall -y Biosig
endif

ifneq (:,/usr/bin/python3)
install :: biosig4c++/python/dist/Biosig-2.1.0.tar.gz
	-/usr/bin/python3 -m pip install $<
uninstall ::
	-/usr/bin/python3 -m pip uninstall -y Biosig
endif

ifneq (:,/usr/bin/R)
first ::
	-make -C biosig4c++/R build
install ::
	-make -C biosig4c++/R install
clean ::
	-make -C biosig4c++/R clean
endif

all: first #win32 win64 #sigviewer #win32/sigviewer.exe win64/sigviewer.exe #biosig_client biosig_server mma java tcl perl php ruby #sigviewer

#---- automatic remaking ---------------------------------------------------#
#   https://www.gnu.org/software/autoconf/manual/autoconf-2.69/html_node/Automatic-Remaking.html#Automatic-Remaking
#---------------------------------------------------------------------------#
$(srcdir)/configure: configure.ac aclocal.m4
	autoconf

# autoheader might not change config.h.in, so touch a stamp file.
$(srcdir)/config.h.in: stamp-h.in
$(srcdir)/stamp-h.in: configure.ac aclocal.m4
	autoheader
	echo timestamp > '$(srcdir)/stamp-h.in'

config.h: stamp-h
stamp-h: config.h.in config.status
	./config.status

Makefile: Makefile.in config.status
	./config.status

config.status: configure
	./config.status --recheck

